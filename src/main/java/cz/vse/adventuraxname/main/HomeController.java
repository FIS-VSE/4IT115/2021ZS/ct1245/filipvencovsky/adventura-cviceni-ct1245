package cz.vse.adventuraxname.main;

import cz.vse.adventuraxname.logika.Hra;
import cz.vse.adventuraxname.logika.IHra;
import cz.vse.adventuraxname.logika.PrikazJdi;
import cz.vse.adventuraxname.logika.Prostor;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.util.HashMap;
import java.util.Map;

public class HomeController implements Pozorovatel {
    @FXML private ImageView hrac;
    @FXML private ListView<Prostor> seznamVychodu;
    @FXML private Button odesli;
    @FXML private TextField vstup;
    @FXML private TextArea vystup;
    private IHra hra;
    private static Map<String, Point2D> souradniceProstoru;

    @FXML
    private void initialize() {
        hra = new Hra();
        hra.getHerniPlan().registruj(this);
        nactiMistnost();
        vystup.setText(hra.vratUvitani()+"\n\n");
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                vstup.requestFocus();
            }
        });
        seznamVychodu.setCellFactory(new Callback<ListView<Prostor>, ListCell<Prostor>>() {
            @Override
            public ListCell<Prostor> call(ListView<Prostor> param) {
                return new ListCell<Prostor>() {
                    @Override
                    protected void updateItem(Prostor item, boolean empty) {
                        super.updateItem(item, empty);
                        if(item==null) {
                            setText("");
                            setGraphic(new ImageView());
                        } else {
                            setText(item.getNazev());
                            ImageView obr = new ImageView("hrac.png");
                            obr.setFitHeight(60);
                            obr.setPreserveRatio(true);
                            setGraphic(obr);
                        }

                    }
                };
            }
        });
    }

    @FXML
    private void zobrazNapovedu() {
        Stage stage = new Stage();

        WebView view = new WebView();
        String urlNapovedy = getClass().getResource("/help.html").toExternalForm();
        view.getEngine().load(urlNapovedy);

        stage.setScene(new Scene(view));
        stage.show();
    }

    private void nactiMistnost() {
        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();
        Point2D souradnice = getSouradniceProstoru().get(aktualniProstor.getNazev());

        seznamVychodu.getItems().addAll(aktualniProstor.getVychody());

        hrac.setLayoutX(souradnice.getX());
        hrac.setLayoutY(souradnice.getY());
    }

    private static Map<String, Point2D> getSouradniceProstoru() {
        if(souradniceProstoru!=null) return souradniceProstoru;
        souradniceProstoru = new HashMap<String, Point2D>();

        souradniceProstoru.put("domeček", new Point2D(14,60));
        souradniceProstoru.put("les", new Point2D(77,28));
        souradniceProstoru.put("hluboký_les", new Point2D(141,61));
        souradniceProstoru.put("jeskyně", new Point2D(141,139));
        souradniceProstoru.put("chaloupka", new Point2D(208,29));

        return souradniceProstoru;
    }

    @FXML
    private void zpracujVstup(ActionEvent actionEvent) {
        String prikaz = vstup.getText();
        zpracujPrikaz(prikaz);
    }

    private void zpracujPrikaz(String prikaz) {
        String vysledek = hra.zpracujPrikaz(prikaz);
        vystup.appendText("> "+prikaz+"\n");
        vystup.appendText(vysledek+"\n\n");
        vstup.clear();

        if(hra.konecHry()) {
            vystup.appendText(hra.vratEpilog());
            vstup.setDisable(true);
            odesli.setDisable(true);
            seznamVychodu.setDisable(true);
        }
    }

    @Override
    public void aktualizuj() {
        seznamVychodu.getItems().clear();
        nactiMistnost();
    }

    public void klikSeznamVychodu(MouseEvent mouseEvent) {
        Prostor novyProstor = seznamVychodu.getSelectionModel().getSelectedItem();
        if(novyProstor==null) return;
        String prikaz = PrikazJdi.NAZEV+" "+novyProstor;
        zpracujPrikaz(prikaz);
    }
}
