package cz.vse.adventuraxname.main;

public interface Pozorovatel {
    /**
     * metoda, kterou volá předmět pozorování (celebrita) při změně (aktualitě)
     */
    void aktualizuj();
}
