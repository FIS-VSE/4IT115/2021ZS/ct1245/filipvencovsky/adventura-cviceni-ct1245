package cz.vse.adventuraxname.main;

public interface PredmetPozorovani {
    /**
     * metoda, skrze kterou se registrují pozorovatelé k odběru aktualit
     */
    void registruj(Pozorovatel pozorovatel);
}
